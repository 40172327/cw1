﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
namespace cw1
{
    //Class created by Lorenzo Dalberto 40172327
    //The student class is used to declares the properties for the Student object.
    //Class last modification: Monday 19/10/2015

    //Student class declared public so that any other class in the project can access its methods
    //and properties
    public class Student
    {
        //Student attributes declared private to make sure they are only available in this class 
        private string firstName;
        private string secondName;
        private string course;
        private int level;
        private int credits;
        private int matricNumber;
        private DateTime dob;

        //minDate and maxDate are used to store realistc values of date of birthdays
        //to be used for extra validation
        private DateTime minDate = DateTime.Parse("01/01/1900");
        private DateTime maxDate = DateTime.Parse("12/12/2016");

        


        // Declaration of FirstName property of type string to be able to manipulate the variable firstName
        public string FirstName
        {
            get { return firstName; }
            set
            {
                //Validation of the firstName propertie, if no value is entered an error message is
                //shown to user
                if (value.Equals(""))
                {

                    throw new ArgumentException("Please enter a valid name");
                }
                firstName = value;
            }
        }

        // Declaration of SecondName property of type string to be able to manipulate the variable secondName
        public string SecondName
        {
            get { return secondName; }
            set
            {
                //Validation of the secondName propertie, if no value is entered an error message
                //is shown to user
                if (value.Equals(""))
                {
                    throw new ArgumentException("Please enter a valid surname");
                }
                secondName = value;
            }
        }

        // Declaration of Dob property of DateTime to be able to manipulate the variable dob
        public DateTime Dob
        {
            get { return dob; }
            set
            {
                DateTime result;
                //Declared a boolean, to check if the date is in the datetime format and in the right range 
                bool correctdate = (DateTime.TryParse(value.ToString(), out result) && result <= maxDate
                   && result >= minDate);
                value = result;
                if (correctdate == false || value.Equals(null))
                {
                    throw new ArgumentException("Please enter a valid date of birth");
                }
                dob = value;
            }
        }

        // Declaration of Course property of type string to be able to manipulate the variable course
        public string Course
        {
            get { return course; }
            set
            {
                //Validation of the Course propertie, if no value is entered an error message is shown to user
                if (value.Equals(""))
                {
                    throw new ArgumentException("Please enter a valid course");
                }
                course = value;
            }
        }

        // Declaration of Matriculation property of type int to be able to manipulate the variable matricNumber
        public int Matriculation
        {
            get
            {
                return matricNumber;
            }
            set
            {
                //Validation of the matriculation propertie, if out of range an error message is shown to user
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("Please enter a valid matriculation number");
                }
                matricNumber = value;
            }
        }


        // Declaration of Level property of type int to be able to manipulate the variable level
        public int Level
        {
            get
            {
                return level;
            }
            set
            {
                //Validation of the level propertie, if out of range an error message is shown to user
                if (value < 1 || value > 4)
                {
                    throw new ArgumentException("Please enter a valid level");
                }
                level = value;
            }
        }

        // Declaration of Credits property of type int to be able to manipulate the variable level
        public int Credits
        {
            get
            {
                return credits;
            }
            set
            {
                //Validation of credit propertie enterd by user
                if (value < 0 || value > 480)
                {
                    //If the value enetered is incorrect than a message is shown on screen
                    throw new ArgumentException("Please enter a valid value for credits");
                }
                credits = value;
            }
        }

        //Declaration of the advance() method: this method updates the student’s level property if 
        //the student has the correct amount of credits. A new window opens up to tell user wheather 
        //he/she will advance to a new level       
        public void advance()
        {
            if (level == 4)
            {
                MessageBox.Show("Student is already in 4th year");
            }
            else if (level == 1 && credits >= 120)
            {
                level++;
                MessageBox.Show("Student is going to 2nd year");
            }
            else if (level == 1 && credits < 120)
            {
                MessageBox.Show("Insufficient credits to move to 2nd year");
            }

            else if (level == 2 && credits >= 240)
            {
                level++;
                MessageBox.Show("Student is going to 3nd year");
            }

            else if (level == 2 && credits < 240)
            {
                MessageBox.Show("Insufficient credits to move to 3rd year");
            }

            else if (level == 3 && credits >= 360)
            {
                level++;
                MessageBox.Show("Student is going to 4th year");
            }
            else if (level == 3 && credits < 360)
            {
                MessageBox.Show("Insufficient credits to move to 4th year");
            }
        }

        //Declaration of the award() method: this method determines if an award is possible for the student.
        //If an award is possible then a new window opens and displays the award type
        public virtual void award()
        {

            if (credits > 0 && credits <= 359)
            {
                String prize = "Certificate of Higher Education ";
                Award a = new Award(firstName, secondName, matricNumber, course, level, prize);
                a.ShowDialog();
            }

            else if (credits > 359 && credits <= 479)
            {
                String prize = "Degree";
                Award a = new Award(firstName, secondName, matricNumber, course, level, prize);
                a.ShowDialog();
            }

            else if (credits > 479)
            {

                String prize = "Honours Degree";
                Award a = new Award(firstName, secondName, matricNumber, course, level, prize);
                a.ShowDialog();
            }
        }
    }
}