﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

//Award method created by Lorenzo Dalberto 40172327
//If the student has enough credits a new window will display the award gained
//Class last modification: Monday 19/10/2015

namespace cw1
{
    /// <summary>
    /// Interaction logic for Award.xaml
    /// </summary>
    public partial class Award : Window
    {
        //The arguments of the Award method are passed from the student class 
        public Award(string name, string surname, int matric, string course, int level, string prize)
        {
            InitializeComponent();
            lblFirstNameAward.Content = name;
            lblSurnameAward.Content = surname;
            lblAwardAward1.Content = prize;
            lblCourseAward1.Content = course;
            lblMatricAward.Content = matric.ToString();
            lblLevelAward.Content = level.ToString();

        }


    }
}
