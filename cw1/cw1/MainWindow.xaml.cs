﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

//Created by Lorenzo Dalberto 40172327
//The main window contains
//Class last modification: Tuesday 22/10/2015



namespace cw1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        public MainWindow()
        {
            InitializeComponent();
        }

        //Instance of Student is created
        Student s = new Student();

        //Instance of Research student is ceated//
        ResearchStudent rs = new ResearchStudent();

        //The chkSupervisor_Checked method display extra fields for the Research Student,
        //Topic and Supervisor
        private void chkSupervisor_Checked(object sender, RoutedEventArgs e)
        {
            //Visibility for topic and supervisor lebals and for topic and supervisor textboxes
            //is set to visible when inserting Research Students details
            lblTopic.Visibility = Visibility.Visible;
            lblSupervisor.Visibility = Visibility.Visible;
            txtTopic.Visibility = Visibility.Visible;
            txtSupervisor.Visibility = Visibility.Visible;
            //Course and Level texboxes are set by default to PhD and 4 as for specification
            txtCourse.Text = "PhD";
           
            //Course and Level texboxes are set to read only so that they can not be edited
            txtCourse.IsReadOnly = true;
            
        }

        //The chkSupervisor_UnChecked method hides extra fields for the Research Student,
        //Topic and Supervisor that are not needed as in this case Student details are being entered
        private void chkSupervisor_Unchecked(object sender, RoutedEventArgs e)
        {
            //Visibility for topic and supervisor lebals and for topic and supervisor textboxes
            //is set to hidden when inserting Students details
            lblTopic.Visibility = Visibility.Hidden;
            lblSupervisor.Visibility = Visibility.Hidden;
            txtTopic.Visibility = Visibility.Hidden;
            txtSupervisor.Visibility = Visibility.Hidden;
            //Course and Level texboxes are cleared of any details containing from the insertion
            //of a possible Research Student details
            txtCourse.Clear();
            txtLevel.Clear();
            //Course and Level texboxes can now be edited
            txtCourse.IsReadOnly = false;
            txtLevel.IsReadOnly = false;
        }



        //The btnSet_Click method on the Set button checks that the correct values are passed from the user 
        //it uses trt/catch blocks to warn user when inappropriate values have been entered
        private void btnSet_Click(object sender, RoutedEventArgs e)
        {

            //When the checkbox is not ticked than values for the student object can be entered
            if (chkSupervisor.IsChecked == false)
            {
                //Exception handling for the student name attribute
                try
                {
                    s.FirstName = txtName.Text;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exception handling for the student surname attribute
                try
                {
                    s.SecondName = txtSecondName.Text;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exception handling for the date of birth attribute
                try
                {
                    s.Dob = DateTime.Parse(txtDob.Text).Date;
                }
                catch (Exception)
                {
                    MessageBox.Show("Please enter a valid date of birth");
                }

                //Exception handling for the student course attribute
                try
                {
                    s.Course = txtCourse.Text;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exception handling for the student matriculation attribute
                try
                {
                    int result;
                    //declare a boolean variable to work with TryParse
                    bool correctmatric = Int32.TryParse(txtMatric.Text, out result);
                    //if a correct value is entered than we assign to the matriculation attribute
                    //otherwise we display an error message to the user
                    s.Matriculation = result;
                    if (correctmatric == false)
                    {
                        throw new ArgumentException("Enter a valid matriculation number");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exception handling for the student result attribute
                try
                {
                    int result;
                    //declare a boolean variable to work with TryParse
                    bool correclevel = Int32.TryParse(txtLevel.Text, out result);
                    //if a correct value is entered than we assign it to the level attribute
                    //otherwise we display an error message to the user from the student class
                    s.Level = result;
                    if (correclevel == false)
                    {
                        throw new ArgumentException("Please enter a valid level");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exception handling for the student credits attribute
                try
                {
                    int result;
                    //declare a boolean variable to work with TryParse
                    bool correctcredits = Int32.TryParse(txtCredits.Text, out result);
                    //if a correct value is entered than we assign it to the credits attribute
                    //otherwise we display an error message to the user from the student class
                    s.Credits = result;
                    if (correctcredits == false)
                    {
                        throw new ArgumentException("Please enter a valid value for credits");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            //When the chckbox is ticked than values for the research student object can be entered
            else
            {

                //Exception handling for research student name attribute
                try
                {
                    rs.FirstName = txtName.Text;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exeption research research student surname attribute
                try
                {
                    rs.SecondName = txtSecondName.Text;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student date of birth attribute
                try
                {
                    DateTime result;
                    bool correctdate = DateTime.TryParse(txtDob.Text, out result);
                    rs.Dob = result;
                    if (correctdate == false)
                    {
                        throw new ArgumentException("Please enter a valid date of birth");
                    }
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }


                //Exception handling for research student course attribute
                try
                {
                    rs.Course = txtCourse.Text;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student matriculation attribute
                try
                {
                    int results;
                    bool correctmatricrs = Int32.TryParse(txtMatric.Text, out results);
                    rs.Matriculation = results;

                    if (correctmatricrs == false)
                    {
                        throw new ArgumentException("Please enter a valid matriculation number");
                    }
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student level attribute
                try
                {
                    rs.Level = int.Parse(txtLevel.Text);
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student level attribute
                try
                {
                    int resultrs;
                    bool correclevelrs = Int32.TryParse(txtLevel.Text, out resultrs);
                    rs.Level = resultrs;
                    if (correclevelrs == false)
                    {
                        throw new ArgumentException("Please enter a valid level");
                    }
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student credits attribute
                try
                {
                    int result;
                    bool correctcredits = Int32.TryParse(txtCredits.Text, out result);
                    rs.Credits = result;
                    if (correctcredits == false)
                    {
                        throw new ArgumentException("Please enter a valid value for credits");
                    }
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student supervisor attribute
                try
                {
                    rs.Supervisor = txtSupervisor.Text;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }

                //Exception handling for research student topic attribute
                try
                {
                    rs.Topic = txtTopic.Text;
                }
                catch (ArgumentException ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        //The clear button removes the contents of the text boxes in the form
        //but doesn't change class's properties
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //clear the text boxes
            txtCourse.Clear();
            txtCredits.Clear();
            txtDob.Clear();
            txtLevel.Clear();
            txtMatric.Clear();
            txtName.Clear();
            txtSecondName.Clear();
            txtTopic.Clear();
            txtSupervisor.Clear();
        }

        //Uses the values of the Student or Research Student classes methods to update the text boxes
        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            //If the checkbox is not clicked than we update the student attributes 
            if (chkSupervisor.IsChecked == false)
            {
                //Assign the value of the course text box to the student attribute course
                txtCourse.Text = s.Course;
                //Assign the value of the name text box to the student attribute name
                txtName.Text = s.FirstName;
                //Assign the value of the surname text box to the student attribute second name
                txtSecondName.Text = s.SecondName;
                //Convert the value in the credit text box to a string and assign it
                //to the student object attribute dob
                txtDob.Text = s.Dob.ToString("d");
                //Convert the value in the level text box to a string and assign it 
                //to the student object attribute level
                txtLevel.Text = s.Level.ToString();
                //Convert the value in the matriculation text box to a string and assign it 
                //to the student object attribute matriculation
                txtMatric.Text = s.Matriculation.ToString();
                //Convert the value in the credit text box to a string and assign 
                //it to the student object attribute credits
                txtCredits.Text = s.Credits.ToString();

            }
            //If the checkbox is clicked than we update the research student attributes 
            else if (chkSupervisor.IsChecked == true)
            {
                //Assign the value of the name text box to the research student attribute name
                txtName.Text = rs.FirstName;
                //Assign the value of the surname text box to the research student attribute second name
                txtSecondName.Text = rs.SecondName;
                //Assign the value of the course text box to the research student attribute course
                txtCourse.Text = rs.Course;
                //Assign the value of the supervisor text box to the research student attribute supervisor
                txtSupervisor.Text = rs.Supervisor;
                //Assign the value of the topic text box to the research student attribute topic
                txtTopic.Text = rs.Topic;
                //Convert the value in the credit text box to a string and assign 
                //it to the research student object attribute credits
                txtCredits.Text = rs.Credits.ToString();
                //Convert the value in the matriculation text box to a string and assign it 
                //to the research student object attribute matriculation
                txtMatric.Text = rs.Matriculation.ToString();
                //Convert the value in the credit text box to a string and assign it
                //to the research student object attribute dob
                txtDob.Text = rs.Dob.ToString("d");
                //Convert the value in the level text box to a string and assign it 
                //to the student object attribute level
                txtLevel.Text = rs.Level.ToString();
            }
        }

        //The award button uses the award() method to determine the award
        private void btnAward_Click(object sender, RoutedEventArgs e)
        {
            //If the checkbox is not clicked the student award() method is invoked 
            if (chkSupervisor.IsChecked == false)
            {
                s.award();
            }
            //If the checkbox is clicked the research student award() method is invoked 
            else if (chkSupervisor.IsChecked == true)
            {
                rs.award();
            }
        }

        //The advance button invokes the advance() method from the student class
        private void btnAdvance_Click(object sender, RoutedEventArgs e)
        {
            if (chkSupervisor.IsChecked == false)
            {
                s.advance();
                txtLevel.Text = s.Level.ToString();
            }
            //If the checkbox is clicked the research student award() method is invoked 
            else if (chkSupervisor.IsChecked == true)
            {
                rs.advance();
                txtLevel.Text = rs.Level.ToString();
            }

            //s.advance();
            //txtLevel.Text = s.Level.ToString();
        }

       

       
    }
}
