﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

//Research Student class created by Lorenzo Dalberto 40172327
//The student class is used to declares the properties for the Research Students object.
//It also inherit properties and methods from the student class
//Class last modification: Monday 19/10/2015

namespace cw1
{
    //Research Student class declared public so that any other class in the project can access its methods
    //and properties. The Class also inherit from inherit properties and methods from the student class
    class ResearchStudent : Student
    {
        //Research Student attributes declared private to make sure they are only available in this class
        private string topic;
        private string supervisor;

        // Declaration of Topic property of type string to be able to manipulate the variable topic
        public string Topic
        {
            get { return topic; }
            set
            {
                //If the value of topic propertie is null than a message is shown to the user in new window
                if (value.Equals(""))
                {
                    throw new ArgumentException("Topic field can not be null");
                }
                topic = value;
            }
        }

        // Declaration of Supervisor property of type string to be able to manipulate the supervisor topic
        public string Supervisor
        {
            get { return supervisor; }
            set
            {
                //If the value of supervisor propertie is null than a message is shown to the user in new window
                if (value.Equals(""))
                {
                    throw new ArgumentException("Supervisor field can not be null");
                }
                supervisor = value;
            }
        }
        //To extend the implementation of the inherited Award() method, it is declared as public override
        public override void award()
        {
            //The award method is overriden as the only award available is 
            //Doctor of Philosophy
            String prize = "Doctor of Philosophy";
            Award researchAward = new Award(FirstName, SecondName, Matriculation, Course, Level, prize);
            researchAward.ShowDialog();
        }
    }
}
